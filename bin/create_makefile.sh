#!/bin/bash

../bin/mkmf

cat << EOF > mm
# Linux-specific options
VERSION=2.0
EXEDIR = ../bin
EXE = gpta.x

# Fortran source code
SRCF90 = \$(wildcard *F90)
OBJ=\$(SRCF90:.F90=.o)

CPPFLAGS=
SRCC=
OBJC=
LDFLAGS=

# Enable single precision - seems to be slower than double precision
# CPPFLAGS += -DSINGLE

# Debugging flags
# CPPFLAGS += -DDEBUG
# CPPFLAGS += -DDEBUG_OPASTE
# CPPFLAGS += -DDEBUG_SELECT

# Ctrl-C interceptor
# CPPFLAGS += -DCSIG
# SRCC = \$(wildcard *.c)
# OBJC = \$(SRCC:.c=.o)

# FFT for IR spectrum
# CPPFLAGS += -DFFT
# LDFLAGS += -ldfftw

# Find symmetry using SPGlib
# CPPFLAGS += -DFIND_SYM
# LDFLAGS += -L../lib -lsymspg

# PLUMED version 2
# CPPFLAGS += -DPLUMED_v2
# LDFLAGS += -lplumed

# Compiling options
help:
	@echo '--------------------------------------------------'
	@echo 'gf         : gfortran compiler - standard build'
	@echo 'ifort      : intel fortran compiler - standard build'
	@echo 'gf-dbg     : gfortran compiler - debugging build'
	@echo 'ifort-dbg  : intel fortran compiler - debugging build'
	@echo 'tar        : create tarball of the sources'
	@echo 'clean      : clean build'
	@echo 'clean-all  : clean everything'
	@echo 'clean-mtd  : clean only the plumed objects'
	@echo '--------------------------------------------------'

# Start up
start: 
	@if [ ! -d ../bin ]; then mkdir ../bin; fi 
	@if [ ! -d ../obj ]; then mkdir ../obj; fi 
	-mv -f ../obj/*.mod ../obj/*.o ./ 
	echo "character(len=255) :: path=\"\$(PWD)\"" > srcdir.in

# Executables
gf: start
	\$(MAKE) FC="gfortran" CC="gcc" \\
	FFLAGS="-funroll-all-loops -O3 -ffast-math -fno-second-underscore" \\
	CFLAGS="-funroll-all-loops -O3 -ffast-math" \\
	\$(EXE) 
	mv \$(EXE) \$(EXEDIR)/

gf-dbg: start
	\$(MAKE) FC="gfortran" CC="gcc" \\
	FFLAGS="-fbounds-check -fbacktrace -fno-second-underscore -ffpe-trap=zero,overflow,invalid" \\
	CFLAGS="-fbounds-check" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

ifort: start
	\$(MAKE) FC="ifort" CC="icc" \\
	FFLAGS="-O3 -ip -w" \\
	CFLAGS="-O3 -ip -w" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

ifort-dbg: start
	\$(MAKE) FC="ifort" CC="icc" \\
	FFLAGS="-debug all -check all -traceback" \\
	CFLAGS="-O3 -ip -w" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

pgi: start
	\$(MAKE) FC="ftn" CC="CC -DCRAY" \\
	FFLAGS="-DCRAY -O3" \\
	CFLAGS="-O3" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

pgi-dbg: start
	\$(MAKE) FC="ftn" CC="CC" \\
	FFLAGS="-O -rm" \\
	CFLAGS="-O -hlist=m -DCRAY" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

clean:
	rm -fr ../obj ./*.o ./*.mod 

clean-all:
	rm -fr ../obj ./*.o ./*.mod ../bin/gpta.x srcdir.in Makefile

tar:    clean-all
	cd ../../ ; tar -zcf gpta.tgz gpta/src gpta/bin gpta/rep gpta/doc ; cd - ; mv ../../gpta.tgz ../
#
# Executable generation
#
\$(EXE): \$(OBJ) \$(OBJC)
	\$(FC) \$(CPPDEFS) \$(CPPFLAGS) \$(FFLAGS) \$(OBJ) \$(OBJC) \$(LDFLAGS) -o \$(EXE)
	-mv *.mod *.o ../obj/.
#
# Plumed compilation rules
#
%.o: %.c
	\$(CC) \$(CFLAGS) -c  -o \$@ \$*.c
#
# Fortran Dependencies
#
EOF

awk '/all:/{for(i=1;i>0;i++){getline;if($1=="SRC")exit;print}}' Makefile >> mm

mv mm Makefile
