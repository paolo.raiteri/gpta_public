#!/bin/bash
cat << EOF
module help_module

interface
  subroutine help(flag)
    use variables, only : ip, dp, command_type, MAXLEN, io
    implicit none
    character(*), intent(in), optional :: flag
  end subroutine help
end interface

end module help_module

subroutine help(flag)

  use variables, only : ip, dp, command_type, MAXLEN, io
  implicit none
  character(*), intent(in), optional :: flag

  integer(ip) :: i
  integer(ip) :: nout
  integer(ip) :: ndash=100
  character(len=MAXLEN) :: fmtd, fmt0, fmt1, fmt2, fmt3

  write(fmtd,*) "(",ndash,"a)"
  fmt0 = "(a)"
  fmt1 = "(/,5x,a)"
  fmt2 = "(10x,a)"
  fmt3 = "(2x,a)"

  write(io,fmtd)("-",i=1,ndash)
  write(io,fmt0)" GPTA 2.0 quick help"
  write(io,fmt0)""
  write(io,fmt0)" Author : Paolo Raiteri - Curtin University"
  write(io,fmt0)" Email  : p.raiteri@curtin.edu.au"
  write(io,fmt0)""
  write(io,fmt0)" gpta.x [-commands] [+flags]"
  write(io,fmt0)""

  if (.not. present(flag)) then
    write(io,fmt0)" List of all available commands"
EOF

awk '/### -/{
  cmd=$2
  printf("    write(io,fmt0)\"    %s\"\n",cmd)
}' ../doc/gpta.md
cat << EOF
    stop
  endif

EOF

awk '/### -/{
  n++
  cmd=$2
  if (n==1){printf("  if (flag==\"%s\" .or. flag==\"%s\")then\n",cmd,substr(cmd,2))}
  else{printf("  elseif (flag==\"%s\" .or. flag==\"%s\")then\n",cmd,substr(cmd,2))}
  spaces=""
  printf("    write(io,fmt0)\"%s\"\n",substr($0,5))
  while(getline){
    if($1=="<br>"){break}
    else{
      if($1=="\`\`\`text"){spaces="\ \ "}
      else if($1=="\`\`\`"){spaces=""}
      else{
        if(length($0)>110){printf("    write(io,fmt0)&\n   \"%s%s\"\n",spaces,$0)}
        else{printf("    write(io,fmt0)\"%s%s\"\n",spaces,$0)}
      }
    }
  }
  printf("\n")
}END{
}' ../doc/gpta.md

cat << EOF
  else
    write(io,fmt0)"  Unknown command "//trim(flag)

  endif

  stop
end subroutine help

subroutine dump_command(comms)
  use variables, only : command_type, io, ip
  implicit none
  type (command_type) :: comms
  integer(ip) :: i, j

  write(io,'(" COMMAND TYPE = " ,    a10  )') comms%type
  write(io,'(" subtype      = " ,    a10  )') comms%subtype
  write(io,'(" inpfile      = " ,    i6   )') comms%inpfile
  write(io,'(" inpfilename  = " ,    a30  )') comms%inpfilename
  write(io,'(" inpfiletype  = " ,    a3   )') comms%inpfiletype
  write(io,'(" outfile      = " ,    i6   )') comms%outfile
  write(io,'(" outfilename  = " ,    a30  )') comms%outfilename
  write(io,'(" outfiletype  = " ,    a3   )') comms%outfiletype
  write(io,'(" posfile      = " ,    a6   )') comms%posfile
  write(io,'(" propndx      = " ,    i6   )') comms%propndx
  write(io,'(" iselect      = " ,    i6   )') comms%iselect
  write(io,'(" nlabel       = " ,    i6   )') comms%nlabel
  write(io,'(" label        = " , 100a4   )') comms%label       (1:comms%nlabel)
  write(io,'(" nprop        = " ,    i6   )') comms%nprop
  write(io,'(" prop         = " , 100f10.5)') comms%prop        (1:comms%nprop )
  write(io,'(" nsel         = " ,    i6   )') comms%nsel
  write(io,'(" sel          = " , 100i6   )') comms%sel         (1:comms%nsel  )
  write(io,'(" nflags       = " ,    i6   )') comms%nflags
  if (comms%nflags>0) then
    do i=1,comms%nflags
      write(io,'("   flags      = " , 100a10  )') comms%flags(i)
      write(io,'("     nparm    = " ,    i6   )') comms%nparm(i)
      if (comms%nparm(i)>0) then
        do j=1,comms%nparm(i)
          write(io,'("       cparm  = " , 100a30  )') comms%cparm(j,i)
        enddo
      endif
    enddo
  endif
  write(io,'(" real         = " ,    f10.5)') comms%real
  write(io,'(" rneigh       = " ,    f10.5)') comms%rneigh
  write(io,'(" dt           = " ,    f10.5)') comms%dt
  write(io,'(" dr           = " ,   3f10.5)') comms%dr
  write(io,'(" pos          = " ,   3f10.5)') comms%pos
  write(io,'(" bounds       = " ,   2f10.5)') comms%bounds
  write(io,'(" mtx          = " ,   9f10.5)') comms%mtx
  write(io,'(" ndim         = " ,    i6   )') comms%ndim
  write(io,'(" vec          = " ,   3f10.5)') comms%vec
  write(io,'(" nbin         = " ,   3i6   )') comms%nbin
  write(io,'(" nvoxels      = " ,   3i6   )') comms%nvoxels
  write(io,'(" avg          = " ,    l    )') comms%avg
  write(io,'(" dump         = " ,    l    )') comms%dump
  write(io,'(" debug        = " ,    l    )') comms%debug
  write(io,'(" origin       = " ,   3f10.5)') comms%origin

  return
end subroutine dump_command
EOF
