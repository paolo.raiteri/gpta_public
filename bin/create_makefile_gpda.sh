#!/bin/bash

../bin/mkmf

cat << EOF > mm
# Linux-specific options
VERSION=2.0
EXEDIR = ../bin
EXE = gpda.x

# Fortran source code
SRCF90 = \$(wildcard *F90)
OBJ=\$(SRCF90:.F90=.o)

# Enable single precision - seems to be slower than double precision
# CPPFLAGS += -DSINGLE

# Debugging flags
# CPPFLAGS += -DDEBUG

# Compiling options
help:
	@echo '--------------------------------------------------'
	@echo 'gf         : gfortran compiler - standard build'
	@echo 'ifort      : intel fortran compiler - standard build'
	@echo 'gf-dbg     : gfortran compiler - debugging build'
	@echo 'ifort-dbg  : intel fortran compiler - debugging build'
	@echo 'tar        : create tarball of the sources'
	@echo 'clean      : clean build'
	@echo 'clean-all  : clean everything'
	@echo 'clean-mtd  : clean only the plumed objects'
	@echo '--------------------------------------------------'

# Start up
start: 
	@if [ ! -d ../bin ]; then mkdir ../bin; fi 
	@if [ ! -d ../gpda_obj ]; then mkdir ../gpda_obj; fi 
	-mv -f ../gpda_obj/*.mod ../gpda_obj/*.o ./ 

# Executables
gf: start
	\$(MAKE) FC="gfortran" CC="gcc" \\
	FFLAGS="-funroll-all-loops -O3 -ffast-math -fno-second-underscore" \\
	CFLAGS="-funroll-all-loops -O3 -ffast-math -DSTANDALONE" \\
	\$(EXE) 
	mv \$(EXE) \$(EXEDIR)/

gf-dbg: start
	\$(MAKE) FC="gfortran" CC="gcc" \\
	FFLAGS="-fbounds-check -fbacktrace -fno-second-underscore -ffpe-trap=zero,overflow,invalid" \\
	CFLAGS="-fbounds-check -DSTANDALONE" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

ifort: start
	\$(MAKE) FC="ifort" CC="icc" \\
	FFLAGS="-O3 -ip -w" \\
	CFLAGS="-O3 -ip -w -DSTANDALONE" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

ifort-dbg: start
	\$(MAKE) FC="ifort" CC="icc" \\
	FFLAGS="-debug all -check all -traceback" \\
	CFLAGS="-O3 -ip -w -DSTANDALONE" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

pgi: start
	\$(MAKE) FC="ftn" CC="CC -DCRAY" \\
	FFLAGS="-DCRAY -O3" \\
	CFLAGS="-O3 -DSTANDALONE" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

pgi-dbg: start
	\$(MAKE) FC="ftn" CC="CC" \\
	FFLAGS="-O -rm" \\
	CFLAGS="-O -hlist=m -DSTANDALONE -DCRAY" \\
	\$(EXE)
	mv \$(EXE) \$(EXEDIR)/

clean:
	rm -fr ../gpda_obj ./*.o ./*.mod 

clean-all:
	rm -fr ../gpda_obj ./*.o ./*.mod ../bin/gpda.x Makefile
#
# Executable generation
#
\$(EXE): \$(OBJ)
	\$(FC) \$(CPPDEFS) \$(CPPFLAGS) \$(FFLAGS) \$(LDFLAGS) \$(OBJ) -o \$(EXE)
	-mv *.mod *.o ../gpda_obj/.
#
# Plumed compilation rules
#
%.o: %.c
	\$(CC) \$(CFLAGS) -c  -o \$@ \$*.c
#
# Fortran Dependencies
#
EOF

awk '/all:/{for(i=1;i>0;i++){getline;if($1=="SRC")exit;print}}' Makefile >> mm

mv mm Makefile
