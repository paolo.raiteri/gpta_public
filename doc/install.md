# GPTA - Installation Manual

GPTA is a pre-/post-processing tool that can be used to manipulate coordinates files.

## Basic installation
You can now download GPTA from a `GitLab` public repository visiting this web site [https://gitlab.com/paolo.raiteri/gpta_public.git](https://gitlab.com/paolo.raiteri/gpta_public.git) or using this command in a terminal window:
```bash
git clone https://gitlab.com/paolo.raiteri/gpta_public.git

```bash
you after you have downloaded the latest version of GPTA from the `git` reposotory you should have a folder called `gpta_public`. If you then navigate to the `src` directory in a terminal window
```bash
cd gpta_public/src
```bash
you can create a Makefile with the correct dependences for the version you have just downloaded
```bash
../bin/create_makefile.sh
```bash
and you should be ready to compile GPTA.

The created `Makefile` contains some standard flags that will allow you to immediately compile GPTA using the `Intel`, `gfortran` or `pgi` compiler using: 
``` bash
make -j4 ifort
```bash
or
``` bash
make -j4 gf
```bash
or
``` bash
make -j4 pgi
```bash
However, if you have a different architecture or want to use different optimisation flags you need to hack the `Makefile` to suit your needs.

Later, if you want to keep yourself up-to-date with all the latest bug fixes and new bugs, you can then simply run
```bash
git pull
```bash
inside the `gpta_public` folder, and recompile GPTA.

## Compilation of additional packages
### Ctr-c intercept
This feature allows you to stop GPTA while processing a trajectory at the end of the current frames without loosing any information.
All the calculated properties will be correctly averaged and printed out.

In order to activate this feature you need to have a C compiler that is compatible with the Fortran compiler used for the compilation of GPTA.
After you have edited the `Makefile` by uncommenting these lines:
```bash
# Ctrl-C interceptor
CPPFLAGS += -DCSIG
SRCC = $(wildcard *.c)
OBJC = $(SRCC:.c=.o)
```

you should be able to simply recompile GPTA running `make gf`, `make ifort` or `make pgi`.

### Plumed 2
This feature allows you to process an atomistic trajectory using any collective variables that can be defined in PLUMED 2.x [http://plumed.github.io/doc-v2.0/user-doc/html/index.html](http://plumed.github.io/doc-v2.0/user-doc/html/index.html).

After you have successfully compiled PLUMED 2.x you need to make sure that the path to the PLUMED 2 library is in you LIBRARY_PATH, e.g.
```bash
export LIBRARY_PATH="$LIBRARY_PATH:/Users/paolo/Programs/plumed2/src/lib/"
```bash

You can then modify the `Makefile` by uncommenting these lines:
```bash
# PLUMED version 2
CPPFLAGS += -DPLUMED_v2
LDFLAGS += -lplumed
```bash
and recompile GPTA. It is advisable to run `make clean` before using the `make gf`, `make ifort` or `make pgi` commands.

### FINDSYM
This package allows for determining the crystal structure of a cell by using the SPG library.

Compile the SPGlib that can be downloaded from [http://spglib.sourceforge.net](http://spglib.sourceforge.net)
You can then modify the `Makefile` by uncommenting these lines:
```bash
# Find symmetry using SPGlib
# CPPFLAGS += -DFIND_SYM
# LDFLAGS += -L../lib -lsymspg
```bash
and recompile GPTA. It is advisable to run `make clean` before using the `make gf`, `make ifort` or `make pgi` commands.
It is assumed that the SPG are installed inside the gpta root directory, you can change that by editing the library location in the `Makefile` (-L.....).
It is however an experimental package which has been poorly tested.
