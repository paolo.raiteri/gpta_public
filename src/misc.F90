subroutine cell_scale(comms)
  use variables, only : ip, dp, command_type, message &
                      , natoms, pos, hmat, hinv, volume
  implicit none

  type(command_type), intent(in) :: comms

  integer(ip) :: i
  real(dp) :: hnew(3,3), hinv_new(3,3)
  real(dp) :: sij(3)

#ifdef DEBUG
  write(0,*)"Entering subroutine :: cell_scale"
#endif

  if ( all(comms%vec(1:3) /= 0.0_dp) )then
    hnew(1:3,1)=comms%vec(1)*hmat(1:3,1)
    hnew(1:3,2)=comms%vec(2)*hmat(1:3,2)
    hnew(1:3,3)=comms%vec(3)*hmat(1:3,3)
  elseif ( comms%mtx(1,1)>0.0_dp .and. comms%mtx(2,2)>0.0_dp .and. comms%mtx(3,3)>0.0_dp ) then
    hnew(1:3,1)=comms%mtx(1:3,1)
    hnew(1:3,2)=comms%mtx(1:3,2)
    hnew(1:3,3)=comms%mtx(1:3,3)
  else
    call message(0_ip,0_ip,0_ip,"No cell scaling defined")
  endif

  call get_3x3_inv (hnew,hinv_new,volume)

  do i=1,natoms
    sij(1) = hinv(1,1)*pos(1,i) + hinv(1,2)*pos(2,i) + hinv(1,3)*pos(3,i)
    sij(2) = hinv(2,1)*pos(1,i) + hinv(2,2)*pos(2,i) + hinv(2,3)*pos(3,i)
    sij(3) = hinv(3,1)*pos(1,i) + hinv(3,2)*pos(2,i) + hinv(3,3)*pos(3,i)

    pos(1,i) = hnew(1,1)*sij(1) + hnew(1,2)*sij(2) + hnew(1,3)*sij(3)
    pos(2,i) = hnew(2,1)*sij(1) + hnew(2,2)*sij(2) + hnew(2,3)*sij(3)
    pos(3,i) = hnew(3,1)*sij(1) + hnew(3,2)*sij(2) + hnew(3,3)*sij(3)
  enddo

  hmat=hnew
  hinv=hinv_new

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: cell_scale"
#endif

  return
end subroutine cell_scale

subroutine distance(comms,dist)
  use variables, only : ip, dp, cp, command_type, property_type, property, message &
                      , natoms, pos, hmat, hinv, iframe, MAXPARM
  use selection_module, only : select_label_for_property, select_indices_for_property, select_selection_for_property
  use pbc_dist_module, only : calc_dist_dis
  implicit none

  type(command_type), intent(in) :: comms
  real(dp), intent(out) :: dist

  integer(ip) :: i, nn
  real(dp) :: pos1(3), pos2(3), dij(3), vec4(4)

  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: distance"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    allocate(prop%idx2(natoms))
  endif

  prop%idx1 = -1_ip
  prop%idx2 = -1_ip

  if (comms%nlabel == 0 .and. comms%ngidx == 0 .and. comms%nsel == 0) then
    prop%idx1 = 1_ip
    prop%idx2 = 2_ip

! Two or more labels
  elseif (comms%nlabel >= 2) then
    call expand_wildcard(comms%label(1),nn,list)
    call select_label_for_property(nn,list,prop%idx1,1_ip)

    do i=2,comms%nlabel
      call expand_wildcard(comms%label(i),nn,list)
      call select_label_for_property(nn,list,prop%idx2,2_ip)
    enddo

! Two or more indices
  elseif (comms%ngidx >= 2) then
    nn=count(comms%gidx(:,1)>0_ip)
    call select_indices_for_property(nn,comms%gidx(1:nn,1),prop%idx1,1_ip)

    do i=2,comms%ngidx
      nn=count(comms%gidx(:,i)>0_ip)
      call select_indices_for_property(nn,comms%gidx(1:nn,i),prop%idx2,2_ip)
    enddo

! Two or more selections
  elseif (comms%nsel >= 2) then
    call select_selection_for_property(comms%sel(1),prop%idx1,1_ip)
    do i=2,comms%nsel
      call select_selection_for_property(comms%sel(i),prop%idx2,2_ip)
    enddo

  else
    call message(0_ip,0_ip,0_ip,"Not enough arguments for the calculation of the distance")
  endif

  nn=0_ip
  pos1=0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    nn=nn+1_ip
    pos1 = pos1 + pos(1:3,i)
  enddo 
  pos1 = pos1 / real(nn,dp)

  nn=0_ip
  pos2=0.0_dp
  do i=1,natoms
    if (prop%idx2(i)/=2) cycle
    nn=nn+1_ip
    pos2 = pos2 + pos(1:3,i)
  enddo 
  pos2 = pos2 / real(nn,dp)
  
  dij(1:3) = pos1(1:3) - pos2(1:3)
  dist = calc_dist_dis(dij)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: distance"
#endif

  return
end subroutine distance

subroutine angle(comms,costheta)
  use variables, only : ip, dp, cp, command_type, property_type, property &
                      , natoms, pos, label, hmat, hinv, MAXPARM, message, iframe
  use selection_module, only : selection_to_indices
  use pbc_dist_module, only : calc_dist_vec
  implicit none
  type(command_type), intent(in) :: comms
  real(dp), intent(out) :: costheta

  integer(ip) :: i, nn
  integer(ip) :: iatm, jatm, katm, latm
  real(dp), dimension(3) :: d1, d2

  real(dp) :: pos1(3), pos2(3), pos3(3), vec4(4)

  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: angle"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    allocate(prop%idx2(natoms))
    allocate(prop%idx3(natoms))
    prop%idx1 = -1_ip
    prop%idx2 = -1_ip
    prop%idx3 = -1_ip

! extract indices
    call selection_to_indices(3_ip,comms,prop)

  endif

  nn=0_ip
  pos1=0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    nn=nn+1_ip
    pos1 = pos1 + pos(1:3,i)
  enddo 
  pos1 = pos1 / real(nn,dp)

  nn=0_ip
  pos2=0.0_dp
  do i=1,natoms
    if (prop%idx2(i)/=2) cycle
    nn=nn+1_ip
    pos2 = pos2 + pos(1:3,i)
  enddo 
  pos2 = pos2 / real(nn,dp)

  nn=0_ip
  pos3=0.0_dp
  do i=1,natoms
    if (prop%idx3(i)/=3) cycle
    nn=nn+1_ip
    pos3 = pos3 + pos(1:3,i)
  enddo 
  pos3 = pos3 / real(nn,dp)

  d1(1:3) = pos1 - pos2
  d2(1:3) = pos3 - pos2

  vec4 = calc_dist_vec(d1) ; d1 = vec4(1:3)
  vec4 = calc_dist_vec(d2) ; d2 = vec4(1:3)

  costheta = dot_product(d1,d2)/sqrt(sum(d1*d1))/sqrt(sum(d2*d2))

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: angle"
#endif

  return
end subroutine angle

subroutine cosine(comms,costheta)
  use variables, only : ip, dp, cp, command_type, property_type, property &
                      , natoms, pos, label, hmat, hinv, MAXPARM, message, iframe
  use selection_module, only : selection_to_indices
  implicit none
  type(command_type), intent(in) :: comms
  real(dp), intent(out) :: costheta

  integer(ip) :: i, nn
  integer(ip) :: iatm, jatm, katm, latm
  real(dp), dimension(3) :: d1, d2

  real(dp) :: pos1(3), pos2(3), pos3(3), pos4(3), vec4(4)

  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: cosine"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    allocate(prop%idx2(natoms))
    allocate(prop%idx3(natoms))
    allocate(prop%idx4(natoms))
    prop%idx1 = -1_ip
    prop%idx2 = -1_ip
    prop%idx3 = -1_ip
    prop%idx4 = -1_ip

! extract indices
    call selection_to_indices(4_ip,comms,prop)

  endif

  nn=0_ip
  pos1=0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    nn=nn+1_ip
    pos1 = pos1 + pos(1:3,i)
  enddo 
  pos1 = pos1 / real(nn,dp)

  nn=0_ip
  pos2=0.0_dp
  do i=1,natoms
    if (prop%idx2(i)/=2) cycle
    nn=nn+1_ip
    pos2 = pos2 + pos(1:3,i)
  enddo 
  pos2 = pos2 / real(nn,dp)

  nn=0_ip
  pos3=0.0_dp
  do i=1,natoms
    if (prop%idx3(i)/=3) cycle
    nn=nn+1_ip
    pos3 = pos3 + pos(1:3,i)
  enddo 
  pos3 = pos3 / real(nn,dp)

  nn=0_ip
  pos4=0.0_dp
  do i=1,natoms
    if (prop%idx4(i)/=4) cycle
    nn=nn+1_ip
    pos4 = pos4 + pos(1:3,i)
  enddo 
  pos4 = pos4 / real(nn,dp)

  d1(1:3) = pos2 - pos1
  d2(1:3) = pos4 - pos3

  costheta = dot_product(d1,d2)/sqrt(sum(d1*d1))/sqrt(sum(d2*d2))

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: cosine"
#endif

  return
end subroutine cosine

subroutine dihedral(comms,phi)
  use variables, only : ip, dp, cp, command_type, property_type, property &
                      , natoms, pos, label, hmat, hinv, MAXPARM, message, iframe
  use selection_module, only : selection_to_indices
  use pbc_dist_module, only : calc_dist_vec
  implicit none
  type(command_type), intent(in) :: comms
  real(dp), intent(out) :: phi

  integer(ip) :: i, nn
  integer(ip) :: iatm, jatm, katm, latm
  real(dp), dimension(3) :: d1, d2

  real(dp) :: pos1(3), pos2(3), pos3(3), pos4(3), vec4(4)
  real(dp), dimension(3) :: d12, d23, d34
  real(dp), dimension(3) :: nvec1, nvec2, nvec3
  real(dp) :: cosphi, sinphi

  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: dihedral"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    allocate(prop%idx2(natoms))
    allocate(prop%idx3(natoms))
    allocate(prop%idx4(natoms))
    prop%idx1 = -1_ip
    prop%idx2 = -1_ip
    prop%idx3 = -1_ip
    prop%idx4 = -1_ip

! extract indices
    call selection_to_indices(4_ip,comms,prop)

  endif

  nn=0_ip
  pos1=0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    nn=nn+1_ip
    pos1 = pos1 + pos(1:3,i)
  enddo 
  pos1 = pos1 / real(nn,dp)

  nn=0_ip
  pos2=0.0_dp
  do i=1,natoms
    if (prop%idx2(i)/=2) cycle
    nn=nn+1_ip
    pos2 = pos2 + pos(1:3,i)
  enddo 
  pos2 = pos2 / real(nn,dp)

  nn=0_ip
  pos3=0.0_dp
  do i=1,natoms
    if (prop%idx3(i)/=3) cycle
    nn=nn+1_ip
    pos3 = pos3 + pos(1:3,i)
  enddo 
  pos3 = pos3 / real(nn,dp)

  nn=0_ip
  pos4=0.0_dp
  do i=1,natoms
    if (prop%idx4(i)/=4) cycle
    nn=nn+1_ip
    pos4 = pos4 + pos(1:3,i)
  enddo 
  pos4 = pos4 / real(nn,dp)

  d12(1:3) = pos2 - pos1
  d23(1:3) = pos3 - pos2
  d34(1:3) = pos4 - pos3

  vec4 = calc_dist_vec(d12) ; d12 = vec4(1:3)
  vec4 = calc_dist_vec(d23) ; d23 = vec4(1:3)
  vec4 = calc_dist_vec(d34) ; d34 = vec4(1:3)

! Get the normal vectors
  call vec_prod(d12,d23,nvec1)
  call vec_prod(d23,d34,nvec2)

! Calculate the dihedral angle
  call vec_prod(nvec1,nvec2,nvec3)
  cosphi = dot_product(nvec1,nvec2)
  sinphi = dot_product(nvec3,d23) / sqrt(sum(d23*d23))
  phi    = atan2(sinphi,cosphi)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: dihedral"
#endif

  return
end subroutine dihedral

subroutine position(comms)
  use variables, only : ip, dp, cp, command_type, iframe, pos, label, charge, hmat &
                      , message, natoms, property_type, property, MAXPARM
  use trajectory_module, only : write_coordinates
  use selection_module, only : selection_to_indices
  implicit none

  type(command_type), intent(in) :: comms

  integer(ip) :: i
  real(dp), dimension(3) :: xcom
  real(dp), allocatable, dimension(:,:) :: pos_tmp
  character(cp), allocatable, dimension(:) :: lab_tmp
  real(dp), allocatable, dimension(:) :: chg_tmp

  integer(ip) :: nn
  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: position"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    prop%idx1 = -1_ip

! extract indices
    call selection_to_indices(1_ip,comms,prop)

  endif 

  if ( any(comms%flags=="traj") ) then

    allocate(pos_tmp(3,natoms))
    allocate(lab_tmp(natoms))
    allocate(chg_tmp(natoms))
    nn=0_ip
    do i=1,natoms
      if (prop%idx1(i)/=1) cycle
      nn=nn+1_ip
      pos_tmp(1:3,nn) = pos(1:3,i)
      lab_tmp(nn) = label(i)
      chg_tmp(nn) = charge(i)
    enddo
    call write_coordinates(comms,iframe,nn,pos_tmp(1:3,1:nn),lab_tmp(1:nn),chg_tmp(1:nn),hmat)
    deallocate(pos_tmp)
    deallocate(lab_tmp)
    deallocate(chg_tmp)

  else

    nn=0_ip
    xcom=0.0_dp
    do i=1,natoms
      if (prop%idx1(i)/=1) cycle
      nn=nn+1_ip
      xcom = xcom + pos(1:3,i)
    enddo 
    write(comms%outfile,'(i6,3x,3f12.5,3x,i6)')iframe,xcom/real(nn,dp),nn

  endif

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: position"
#endif

  return
end subroutine position

subroutine gyration(comms,rgyr)
  use variables, only : ip, dp, cp, command_type, iframe, pos, label, charge, hmat &
                      , message, natoms, property_type, property, MAXPARM
  use trajectory_module, only : write_coordinates
  use selection_module, only : selection_to_indices
  implicit none

  type(command_type), intent(in) :: comms
  real(dp), intent(out) :: rgyr

  integer(ip) :: i
  real(dp), dimension(3) :: xcom

  integer(ip) :: nn
  character(cp), dimension(MAXPARM) :: list
  type(property_type), pointer :: prop

#ifdef DEBUG
  write(0,*)"Entering subroutine :: position"
#endif

  prop => property(comms%propndx)

  if (iframe==1_ip) then
    allocate(prop%idx1(natoms))
    prop%idx1 = -1_ip

! extract indices
    call selection_to_indices(1_ip,comms,prop)

  endif 

! centre of mass
  nn=0_ip
  xcom=0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    nn=nn+1_ip
    xcom = xcom + pos(1:3,i)
  enddo 
  xcom = xcom / real(nn,dp)

! gyration radius
  rgyr = 0.0_dp
  do i=1,natoms
    if (prop%idx1(i)/=1) cycle
    rgyr = rgyr + sum((pos(1:3,i)-xcom(1:3))**2)
  enddo 
  rgyr = sqrt(rgyr/real(nn,dp))

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: position"
#endif

  return
end subroutine gyration

subroutine  rotate_molecule(vec,theta,natoms,pos,pivot)
  use variables, only : ip, dp
  implicit none
  real(dp), intent(in) :: vec(3), theta
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(inout) :: pos
  integer, intent(in) :: pivot

  integer(ip) :: i
  real(dp) :: v(3), v2(3), costheta, sintheta, u(3), rcom(3)
  real(dp), dimension (3,3) :: rotmat
  real(dp), dimension (0:3) :: q
  real(dp) :: norm, theta2

#ifdef DEBUG
  write(0,*)"Entering subroutine :: rotate_molecule"
#endif

  norm=sqrt(sum(vec*vec))
  v=vec/norm
  v2=v*v
  costheta=cos(theta)
  sintheta=sin(theta)

  rotmat(1,1)=v2(1)+(1.-v2(1))*costheta
  rotmat(1,2)= v(1)*v(2)*(1-costheta)-v(3)*sintheta
  rotmat(1,3)= v(1)*v(3)*(1-costheta)+v(2)*sintheta

  rotmat(2,1)= v(1)*v(2)*(1-costheta)+v(3)*sintheta
  rotmat(2,2)=v2(2)+(1.-v2(2))*costheta
  rotmat(2,3)= v(2)*v(3)*(1-costheta)-v(1)*sintheta

  rotmat(3,1)= v(1)*v(3)*(1-costheta)-v(2)*sintheta
  rotmat(3,2)= v(2)*v(3)*(1-costheta)+v(1)*sintheta
  rotmat(3,3)=v2(3)+(1.-v2(3))*costheta

  if (pivot<1_ip .or. pivot>natoms) then
    rcom=0.0_dp
    do i=1,natoms
      rcom=rcom+pos(1:3,i)
    enddo
    rcom=rcom/real(natoms,dp)
  else
    rcom = pos(1:3,pivot)
  endif

  do i=1,natoms
    u=pos(1:3,i)-rcom(1:3)
    pos(1:3,i)=matmul(rotmat,u(1:3))+rcom(1:3)
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: rotate_molecule"
#endif

  return
end subroutine rotate_molecule

subroutine centre_solute(comms)
  use variables, only : ip, cp, dp, command_type, message, natoms, pos, label, hmat, hinv &
                      , ldo_cluster, property_type, MAXPARM, property
  use selection_module, only : select_label_for_property, select_indices_for_property, select_selection_for_property
  implicit none

  type(command_type), intent(in) :: comms
 
  integer(ip) :: i,nn
  real(dp), dimension(3) :: rcom, dij, sij, centre, pos0
  real(dp), dimension(3), save :: xcom0
  logical, save :: first_time_in=.true.
  
  type(property_type), pointer :: prop
  character(cp), dimension(MAXPARM) :: list

#ifdef DEBUG
  write(0,*)"Entering subroutine :: centre_solute"
#endif

  prop => property(comms%propndx)

  if (any(comms%vec > 0.0_dp)) then
    centre = comms%vec(1:3)
  elseif (any(comms%pos > 0.0_dp)) then
    centre = comms%pos(1:3)
  elseif (any(comms%flags == "origin")) then
    centre = 0.0_dp
  elseif (any(comms%flags == "fix")) then
    if (.not. first_time_in) centre=xcom0
  elseif (any(comms%flags == "centre") .or. any(comms%flags=="center")) then
    do i=1,3
      centre(i) = 0.5_dp*hmat(i,i) 
    enddo
  else
    call message(0_ip,0_ip,0_ip,"Don't know where to locate the atoms' centre of mass")
  endif

  if (prop%first) then
    prop%first=.false.
    allocate(prop%idx1(natoms))
    
    prop%idx1 = -1_ip

    if (comms%nlabel > 0_ip) then
      do i=1,comms%nlabel
        call expand_wildcard(comms%label(i),nn,list)
        call select_label_for_property(nn,list,prop%idx1,1_ip)
      enddo

    elseif (comms%ngidx > 0_ip) then
      do i=1,comms%ngidx
        nn=count(comms%gidx(:,i)>0_ip)
        call select_indices_for_property(nn,comms%gidx(1:nn,i),prop%idx1,1_ip)
      enddo

    elseif (comms%nsel > 0_ip) then
      do i=1,comms%nsel
        call select_selection_for_property(comms%sel(i),prop%idx1,1_ip)
      enddo

    else
      call message(0_ip,0_ip,0_ip,"Don't know which species to centre")
    endif
 
    prop%nspecies1 = count(prop%idx1==1)

  endif

!  nn=0_ip
!  rcom = 0.0_dp
!  do i=1,natoms
!    if (prop%idx1(i) == 1_ip) then
!      if (nn==0_ip) then
!        pos0(1:3) = pos(1:3,i)
!        nn=1_ip
!      else
!        dij=pos(1:3,i)-pos0(1:3)
!        if (.not. ldo_cluster) then
!          sij(1) = hinv(1,1)*dij(1) + hinv(1,2)*dij(2) + hinv(1,3)*dij(3)
!          sij(2) = hinv(2,1)*dij(1) + hinv(2,2)*dij(2) + hinv(2,3)*dij(3)
!          sij(3) = hinv(3,1)*dij(1) + hinv(3,2)*dij(2) + hinv(3,3)*dij(3)
!          sij(1:3) = sij(1:3)-nint(sij(1:3))
!          dij(1) = hmat(1,1)*sij(1) + hmat(1,2)*sij(2) + hmat(1,3)*sij(3)
!          dij(2) = hmat(2,1)*sij(1) + hmat(2,2)*sij(2) + hmat(2,3)*sij(3)
!          dij(3) = hmat(3,1)*sij(1) + hmat(3,2)*sij(2) + hmat(3,3)*sij(3)
!        endif
!
!        rcom(1:3) = rcom(1:3) + dij(1:3)
!        nn = nn + 1_ip
!      endif
!    endif
!  enddo
!  rcom = pos0 + rcom / real(nn,dp)

  nn=0_ip
  rcom = 0.0_dp
  do i=1,natoms
    if (prop%idx1(i) == 1_ip) then
      rcom(1:3) = rcom(1:3) + pos(1:3,i)
      nn = nn + 1_ip
    endif
  enddo
  rcom = rcom / real(nn,dp)

  if (any(comms%flags == "fix") .and. first_time_in) then
    centre=rcom
    xcom0=centre
    first_time_in=.false.
  endif

  do i=1,3
    dij(i) = centre(i) - rcom(i)
  enddo

  do i=1,natoms
    pos(1:3,i) = pos(1:3,i) + dij(1:3)
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: centre_solute"
#endif

  return
end subroutine centre_solute

subroutine unwrap(comms)
  use variables, only : ip, dp, command_type, message, natoms, pos, hmat, hinv
  implicit none

  type(command_type), intent(in) :: comms
 
  integer(ip) :: i, iatm
  real(dp), dimension(3) :: dij, sij
  real(dp), save, allocatable, dimension (:,:) :: old_pos

  logical, save :: first_time_in=.true.
  
#ifdef DEBUG
  write(0,*)"Entering subroutine :: unwrap"
#endif

  if (first_time_in) then
    first_time_in = .false.
    allocate(old_pos(3,natoms))
    old_pos(1:3,1:natoms) = pos(1:3,1:natoms)
    return
  endif

  do iatm=1,natoms
    dij(1:3) = pos(1:3,iatm) - old_pos(1:3,iatm)
    sij(1) = hinv(1,1)*dij(1) + hinv(1,2)*dij(2) + hinv(1,3)*dij(3)
    sij(2) = hinv(2,1)*dij(1) + hinv(2,2)*dij(2) + hinv(2,3)*dij(3)
    sij(3) = hinv(3,1)*dij(1) + hinv(3,2)*dij(2) + hinv(3,3)*dij(3)
    sij(1:3) = sij(1:3)-anint(sij(1:3))
    dij(1) = hmat(1,1)*sij(1) + hmat(1,2)*sij(2) + hmat(1,3)*sij(3)
    dij(2) = hmat(2,1)*sij(1) + hmat(2,2)*sij(2) + hmat(2,3)*sij(3)
    dij(3) = hmat(3,1)*sij(1) + hmat(3,2)*sij(2) + hmat(3,3)*sij(3)
    pos(1:3,iatm) = old_pos(1:3,iatm) + dij(1:3)
  enddo
  old_pos(1:3,1:natoms) = pos(1:3,1:natoms)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: unwrap"
#endif

  return
end subroutine unwrap
