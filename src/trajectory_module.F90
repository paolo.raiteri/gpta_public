module trajectory_module
  use variables, only : ip, dp, cp, ft, iverbose, message, lneigh_done
  use system_module, only : atm2mol
  implicit none

contains

  subroutine get_number_of_atoms(io,ftype,nn)
    use variables, only : natoms0
    implicit none
    integer(ip), intent(in) :: io
    character(len=ft), intent(inout) :: ftype
    integer(ip), intent(out) :: nn

#ifdef DEBUG
    write(0,*)"Entering subroutine :: get_number_of_atoms"
#endif

    if (ftype=="glp") ftype="gin"
    if (ftype=="res") ftype="gin"

    select case (ftype)
! ________________________________________________________________________________ !
! Unknown file type - stop
      case default
        call message(0_ip,0_ip,0_ip,"unknown input file type",ftype)
! ________________________________________________________________________________ !
! XYZ 
      case ("xyz")
        read(io,*,err=100) nn
! ________________________________________________________________________________ !
! PDB 
      case ("pdb")
        call get_natom_pdb(io,nn)
! ________________________________________________________________________________ !
! DL_POLY 2.X
      case ("dlp")
        call get_natom_dlp(io,nn)
! ________________________________________________________________________________ !
! LAMMPS
      case ("lmp")
        call get_natom_lmp(io,nn)
! ________________________________________________________________________________ !
! LAMMPS trajectory
      case ("ltr")
        call get_natom_lmptrj(io,nn)
! ________________________________________________________________________________ !
! GULP
      case ("gin")
        call get_natom_gulp(io,nn)
! ________________________________________________________________________________ !
! GROMACS
      case ("gro")
        call get_natom_gro(io,nn)
! ________________________________________________________________________________ !
! CIF 
      case ("cif")
        call get_natom_cif(io,nn)
! ________________________________________________________________________________ !
! Gaussian
      case ("gau")
        call get_natom_gauss(io,nn)
! ________________________________________________________________________________ !
! NWChem
      case ("nwc")
        call get_natom_nwchem(io,nn)
! ________________________________________________________________________________ !
! Quantum Espresso
      case ("qeout")
        call get_natom_qeout(io,nn)
! ________________________________________________________________________________ !
! Accelyres arc file
      case ("arc")
        call get_natom_arc(io,nn)
! ________________________________________________________________________________ !
! MOL2 file
      case ("mol2")
        call get_natom_mol2(io,nn)
! ________________________________________________________________________________ !
! SIESTA
      case ("fdf")
        call get_natom_siesta(io,nn)
! ________________________________________________________________________________ !
! AMBER prepi file
      case ("prp")
        call get_natom_prepi(io,nn)
! ________________________________________________________________________________ !
! AMBER restart file
      case ("rst")
        nn=natoms0
! ________________________________________________________________________________ !
! DCD CHARMM
      case ("dcd")
        nn=natoms0
    end select
!
    if (nn==0_ip) call message(0_ip,0_ip,0_ip,"No atoms found")
    rewind(io)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: get_number_of_atoms"
#endif

    return
100 call message(0_ip,0_ip,0_ip,"Cannot read number of atoms in XYZ")
  end subroutine get_number_of_atoms

  subroutine read_coordinates(io,ftype,first,nn,xx,lbl,hh,go,chg,ctype)
    use variables, only : label0
    implicit none
    integer(ip), intent(in) :: io
    character(ft), intent(in) :: ftype
    logical, intent(in) :: first
    integer(ip), intent(in) :: nn
    real(dp), intent(out) :: xx(3,nn)
    character(cp), intent(out) :: lbl(nn)
    real(dp), intent(out) :: hh(3,3)
    logical, intent(out) :: go
    real(dp), dimension(nn), intent(inout) :: chg
    character(len=4) :: ctype

    integer(ip) :: iatm
    real(dp) :: dij(3)
    real(dp), dimension(6) :: cell
    logical, save :: first_time_in=.true.

#ifdef DEBUG
    write(0,*)"Entering subroutine :: read_coordinates"
#endif

    lneigh_done=.false.

    select case (ftype)
! ________________________________________________________________________________ !
! Unknown file type - stop
      case default
        call message(0_ip,0_ip,0_ip,"unknown input file type",ftype)
! ________________________________________________________________________________ !
! XYZ 
      case ("xyz")
        call read_xyz(io,nn,xx,lbl,chg,go)
! ________________________________________________________________________________ !
! PDB 
      case ("pdb")
        call read_pdb(io,nn,xx,lbl,chg,cell,go)
        if(go .and. (sum(cell(1:3))>1.0_dp)) call get_hmat(cell,hh)
! ________________________________________________________________________________ !
! DL_POLY 2.X
      case ("dlp")
        call read_dlp(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! LAMMPS
      case ("lmp")
        call read_lmp(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! LAMMPS trajectory
      case ("ltr")
        call read_lmptrj(io,nn,xx,lbl,chg,hh,go)
! ________________________________________________________________________________ !
! GULP
      case ("gin")
        call read_gulp(io,nn,xx,lbl,hh,go,chg)
! ________________________________________________________________________________ !
! GROMACS
      case ("gro")
        call read_gro(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! CIF
      case ("cif")
        call read_cif(io,nn,xx,lbl,chg,hh,go)
! ________________________________________________________________________________ !
! SIESTA
      case ("fdf")
        call read_siesta(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! Gaussian
      case ("gau")
        hh=0.0_dp
        call read_gauss(io,nn,xx,lbl,go,chg)
! ________________________________________________________________________________ !
! NWChem
      case ("nwc")
        hh=0.0_dp
        call read_nwchem(io,nn,xx,lbl,go)
! ________________________________________________________________________________ !
! Quantum Espresso
      case ("qeout")
        call read_qeout(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! Accelyres arc file
      case ("arc")
        call read_arc(io,nn,xx,lbl,hh,go)
! ________________________________________________________________________________ !
! MOL2 file
      case ("mol2")
        call read_mol2(io,nn,xx,lbl,hh,go,chg)
! ________________________________________________________________________________ !
! AMBER prepi file
      case ("prp")
        call read_amber_prepi(io,nn,xx,lbl,chg,go)
! ________________________________________________________________________________ !
! AMBER restart file
      case ("rst")
        call read_amber(io,first,nn,xx,hh,go)
        if (go) lbl(1:nn)=label0(1:nn)
! ________________________________________________________________________________ !
! DCD CHARMM 
      case ("dcd")
        call read_dcd(io,first,nn,xx,hh,go)
        if (go) lbl(1:nn)=label0(1:nn)
    end select

! Convert fractional to cartesian
    if (ctype == 'frac') then
      do iatm=1,nn
        dij(1:3) = xx(1:3,iatm)
        xx(1,iatm) = hh(1,1)*dij(1) + hh(1,2)*dij(2) + hh(1,3)*dij(3)
        xx(2,iatm) = hh(2,1)*dij(1) + hh(2,2)*dij(2) + hh(2,3)*dij(3)
        xx(3,iatm) = hh(3,1)*dij(1) + hh(3,2)*dij(2) + hh(3,3)*dij(3)
      enddo
    endif

! Save labels for DCD 
    if (first_time_in .and. first) then
      first_time_in = .false.
      label0(1:nn)=lbl(1:nn)
    endif

! This overrides the labels with those in of the first frame
    if (go) lbl(1:nn)=label0(1:nn)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_coordinates"
#endif

    return
  end subroutine read_coordinates

  subroutine write_coordinates(comms,iframe,nn,inp_coord,lbl,chg,hh)
    use variables, only : lmolecule, iverbose, ldo_cluster, command_type
    use topology_module, only : calc_covalent_list, merge_covalent_list, write_connectivity
    implicit none
    type(command_type), intent(in) :: comms
    integer(ip), intent(in) :: iframe
    integer(ip), intent(in) :: nn
    real(dp), dimension(3,nn), intent(in) :: inp_coord
    character(cp), dimension(nn), intent(in) :: lbl
    real(dp), dimension(nn), intent(in) :: chg
    real(dp), dimension(3,3), intent(in) :: hh

    integer(ip) :: io
    character(ft) :: ftype
    character(len=6) :: posfile
    character(len=4) :: coordtype
    real(dp), dimension(3,nn) :: coord

    integer(ip) :: iatm
    integer(ip) :: iout

    real(dp) :: hinv(3,3), rtmp, sij(3)

    interface
      subroutine write_pdb(uout,natoms,coord,lbl,charge,hmat,frame,atm2mol,lcon)
        use variables, only : ip, dp, cp, pi
        implicit none
        integer(ip), intent(in) :: uout
        integer(ip), intent(in) :: natoms
        real(dp), dimension(3,natoms), intent(in) :: coord
        character(cp), dimension(natoms), intent(in) :: lbl
        real(dp), dimension(natoms), intent(in) :: charge
        real(dp), intent(in) :: hmat(3,3)
        integer(ip), intent(in) :: frame
        integer(ip), dimension(natoms), intent(in), optional :: atm2mol
        logical, intent(in), optional :: lcon
      end subroutine write_pdb
    end interface

#ifdef DEBUG
    write(0,*)"Entering subroutine :: write_coordinates"
#endif

    io = comms%outfile
    ftype = comms%outfiletype
    posfile = comms%posfile
    coordtype = comms%coordtype

    if (comms%coordtype == "frac") then
      call get_3x3_inv(hh,hinv,rtmp)
      do iatm=1,nn
        coord(1,iatm) = hinv(1,1)*inp_coord(1,iatm) + hinv(1,2)*inp_coord(2,iatm) + hinv(1,3)*inp_coord(3,iatm)
        coord(2,iatm) = hinv(2,1)*inp_coord(1,iatm) + hinv(2,2)*inp_coord(2,iatm) + hinv(2,3)*inp_coord(3,iatm)
        coord(3,iatm) = hinv(3,1)*inp_coord(1,iatm) + hinv(3,2)*inp_coord(2,iatm) + hinv(3,3)*inp_coord(3,iatm)
      enddo
    elseif (comms%coordtype == "cart") then
      coord=inp_coord
    else
      call message(0_ip,0_ip,0_ip,"Unknown coordinates type (use 'cart' or 'frac'")
    endif

    select case (ftype)
! ________________________________________________________________________________ !
! Unknown file type - stop
      case default
        call message(0_ip,0_ip,0_ip,"Unknown output file type",ftype)
! ________________________________________________________________________________ !
! XYZ 
      case ("xyz")
        write(io,*) nn
        write(io,'("BOX",3f10.5)')hh(1,1),hh(2,2),hh(3,3)
          do iatm=1,nn
            write(io,'(a4,4(1x,f15.10))')lbl(iatm),coord(1:3,iatm)!,chg(iatm)
          enddo
! ________________________________________________________________________________ !
! GULP
      case ("gin")
        write(io,'(a)')'sing conv'
        write(io,'(a)')'vectors'
        write(io,'(3f15.10)')hh(1:3,1)
        write(io,'(3f15.10)')hh(1:3,2)
        write(io,'(3f15.10)')hh(1:3,3)
        if (comms%coordtype == "frac") then
          write(io,'(a)')'fractional'
        elseif (comms%coordtype == "cart") then
          write(io,'(a)')'cartesian'
        endif
        do iatm=1,nn
          write(io,'(a4," core ",3(1x,f15.10),3x,f8.5)')lbl(iatm),coord(1:3,iatm),chg(iatm)
        enddo
! ________________________________________________________________________________ !
! PDB 
      case ("pdb")
        if (allocated(atm2mol)) then
          call write_pdb(io,nn,coord,lbl,chg,hh,iframe,atm2mol)
        else
          call write_pdb(io,nn,coord,lbl,chg,hh,iframe)
        endif
! ________________________________________________________________________________ !
! PDB - with connectivity
      case ("pdb2")
        call write_pdb(io,nn,coord,lbl,chg,hh,iframe,atm2mol,.true.)
! ________________________________________________________________________________ !
! GROMACS
      case ("gro")
        if (lmolecule) then
          call write_gro(io,nn,coord,lbl,hh,iframe,atm2mol)
        else
          call message(0_ip,0_ip,0_ip,"Need to specify the topology for GROMACS output")
        endif
! ________________________________________________________________________________ !
! DCD Charm 
      case ("dcd")
        call write_dcd(io,nn,coord,hh,posfile)
! ________________________________________________________________________________ !
! MOL2
      case ("mol2")
        call write_mol2(io,nn,coord,lbl,hh,chg)
! ________________________________________________________________________________ !
! DL_POLY 2.XX
      case ("dlp")
        call write_dlp(io,nn,coord,lbl,hh,iframe)
      case ("cfg")
        call write_dlp(io,nn,coord,lbl,hh,iframe)
      case ("hst")
        call write_dlp_hist(io,nn,coord,lbl,hh,iframe)

    end select

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: write_coordinates"
#endif
    return
    
  end subroutine write_coordinates

end module trajectory_module

subroutine get_number_of_atoms_per_kind()
  use variables, only : ip, dp, cp, iverbose, message, iframe &
                      , natoms, label &
                      , num_atoms_kinds, atoms_kind_list, num_atoms_of_kind, atom_kind_id
  implicit none
  integer(ip) :: iatm, il
  integer(ip) :: itype
  logical :: found
  logical, save :: first_time_in=.true.
  character(len=20) :: str

#ifdef DEBUG
  write(0,*)"Entering subroutine :: get_number_of_atoms_per_kind"
#endif

  if (first_time_in) then
    allocate(atoms_kind_list  (natoms))
    allocate(num_atoms_of_kind(natoms))
    allocate(atom_kind_id     (natoms))
  endif

  if (natoms > size(atom_kind_id)) then
    deallocate(atoms_kind_list)
    deallocate(num_atoms_of_kind)
    deallocate(atom_kind_id)
    allocate(atoms_kind_list  (natoms))
    allocate(num_atoms_of_kind(natoms))
    allocate(atom_kind_id     (natoms))
  endif

  num_atoms_kinds=0_ip
  atoms_kind_list=''
  num_atoms_of_kind=0_ip
  atom_kind_id=0_ip

  do iatm=1,natoms
    found=.false.
    do il=1,num_atoms_kinds
      if(label(iatm)==atoms_kind_list(il))then
        found=.true.
        exit
      endif
    enddo
    if(.not.found)then
      num_atoms_kinds = num_atoms_kinds + 1_ip
      atoms_kind_list(num_atoms_kinds) = label(iatm)
      num_atoms_of_kind (num_atoms_kinds) = num_atoms_of_kind (num_atoms_kinds) + 1_ip
      atom_kind_id(iatm) = num_atoms_kinds
    else
      num_atoms_of_kind(il) = num_atoms_of_kind(il) + 1_ip
      atom_kind_id(iatm) = il
    endif
  enddo

  if(iverbose>0_ip .and. first_time_in) then
    call message(1_ip,0_ip,0_ip,"Number of Atoms found",natoms)
    do itype=1,num_atoms_kinds
       write(str,'(a4," -> ",i8)')atoms_kind_list(itype),num_atoms_of_kind(itype)
       call message(1_ip,itype,num_atoms_kinds,"Species found",str)
    enddo
  endif

  first_time_in=.false.

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: get_number_of_atoms_per_kind"
#endif

  return
end subroutine get_number_of_atoms_per_kind

subroutine read_xyz(io,natoms,pos,label,charge,go)
  use variables, only : ip, dp, cp, ldo_cluster
  implicit none
  integer(ip), intent(in) :: io
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(out) :: pos
  character(cp), dimension(natoms), intent(out) :: label
  real(dp), dimension(natoms), intent(out) :: charge
  logical, intent(out) :: go

  character(len=200) :: line
  integer(ip) :: i
  logical :: lchg

#ifdef DEBUG
  write(0,*)"Entering subroutine :: read_xyz"
#endif

  go=.true.
    
!  ldo_cluster=.true.
  read(io,*,err=200,end=200)i
  read(io,*,err=200,end=200)
  read(io,'(a200)',err=200,end=200)line

  read(line,*,err=201,end=201)label(1),pos(1:3,1),charge(1)
  do i=2,natoms
    read(io,*,err=200,end=200)label(i),pos(1:3,i),charge(i)
  enddo
  
  return

201 read(line,*)label(1),pos(1:3,1) 
  do i=2,natoms
    read(io,*,err=200,end=200)label(i),pos(1:3,i)
    charge(i)=0.0_dp
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_xyz"
#endif
  return

200 go=.false.
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_xyz"
#endif
  return
end subroutine read_xyz 
