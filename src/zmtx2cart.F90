module zmtx2cart

contains
! 
! This file is part of the SIESTA package.
!
! Copyright (c) Fundacion General Universidad Autonoma de Madrid:
! E.Artacho, J.Gale, A.Garcia, J.Junquera, P.Ordejon, D.Sanchez-Portal
! and J.M.Soler, 1996- .
! 
! Use of this software constitutes agreement with the full conditions
! given in the SIESTA license, as signed by all legitimate users.
!
!**************************************************************************
!  This module contains subroutines required for Z-matrix based geometry  *
!  optimisation in SIESTA.                                                *
!                                                                         *
!  Written by Rainer Hoft (UTS) & Julian Gale (Curtin), March - May 2005  *
!**************************************************************************

      subroutine Z2CGen(atomNr,r,theta,phi,RelatedC,x,y,z,phi_ref)
!
!  Subroutine for generation of cartesian coordinates from Z-matrix
!  given the following special cases:
!    - the first entry is pure cartesian coordinates
!    - the second entry is spherical coordinates relative to the first atom
!    - the third entry is general Z-matrix coordinates, but with the torsion atom 
!      a dummy atom 1 unit in the z direction above the second
!    - the fourth entry onwards are general Z-matrix coordinates
!
       use variables, only : dp, pi
       implicit none
!
!  Passed variables
!
        integer,  intent(in)     :: atomNr
        real(dp), intent(in)     :: r
        real(dp), intent(in)     :: theta
        real(dp), intent(in)     :: phi
        real(dp), intent(inout)  :: RelatedC(3,3)
        real(dp), intent(out)    :: x
        real(dp), intent(out)    :: y
        real(dp), intent(out)    :: z
!
        real(dp), intent(in)     :: phi_ref
        real(dp), dimension(3), parameter :: ex = (/ 1.0_dp, 0.0_dp, 0.0_dp /), ez = (/ 0.0_dp, 0.0_dp, 1.0_dp /)

        real(dp), dimension(3)   :: third_atom_coords, tac2
        real(dp), dimension(3)   :: unit_vector, r2
        real(dp) :: r2_mod,  angle_2

        real(dp) :: r_1, phi_1, theta_1
        real(dp) :: x_0, y_0, z_0

        if (atomNr.eq.0) then
          x = r
          y = theta
          z = phi
        elseif (atomNr.eq.1) then

          x = r*sin(theta)*cos(phi) + RelatedC(1,1)
          y = r*sin(theta)*sin(phi) + RelatedC(2,1)
          z = r*cos(theta) + RelatedC(3,1)

        elseif (atomNr.eq.2) then

                x_0 = RelatedC(1,2)
                y_0 = RelatedC(2,2)
                z_0 = RelatedC(3,2)
!               Note, the atom this atom binds to and the previous one
                r2 = RelatedC(:,1) - RelatedC(:,2)

                r2_mod = sqrt(dot_product(r2,r2))
                theta_1 = acos(r2(3)/r2_mod)        ! Could this be not general enough?
                ! Theta should be always between 0 and pi.
                ! If it is not, it should be forced to be, changing its sign and
                ! changing phi to phi+/- pi
                ! This is not currently enforced!

                if ( (abs(r2(2)) .gt. 1.0e-8_dp) .and.  (abs(r2(1)) .gt. 1.0e-8_dp)) then
                   ! Compute explicitly the phi angle

                   phi_1 = atan2(r2(2),r2(1))

                else
                   ! Take it from the Zmatrix itself
                   phi_1 = phi_ref
                endif

                r_1 = r2_mod

          ! Change temporarily the  coordinates of the reference
          ! atoms for the third atom to:

          ! Atom2 along the x axis, at a distance r_1 from the origin.
          ! Remember that RelatedC(:,1) refers to the atom to which this
          ! one "binds" (the "i" in the "i j k" in the Zmatrix spec)
          RelatedC(:,1) = r_1 * ex

          ! Atom 1 at the origin
          RelatedC(:,2) = 0.0_dp  ! Note the index: atom1 is now j

          ! Fake reference atom at (0,0,1) over the second 
          ! Atom k in the Zmatrix spec
          RelatedC(:,3) = RelatedC(:,1)  + ez

          call Z2C(r,theta,phi,RelatedC,x,y,z)
!
! We need to rotate and translate the third atom

           ! Original (reduced) positions

            third_atom_coords(1:3) = (/ x, y, z /)  !just computed above

          ! First, rotate by phi_1 around the z axis.
          ! Use Rodrigues formula (see 'rotation matrix' in Wikipedia)
            third_atom_coords(1:3) = rotate(third_atom_coords,ez,phi_1)

            ! New rotation axis defined by a vector lying on the xy
            ! plane, forming a phi_1-90 degree angle with the x axis:

            unit_vector = rotate(ex,ez, phi_1-pi/2.0_dp)

            ! Rotate 
            ! with the unit_vector as axis and an angle of 90-theta_1

            angle_2 = pi/2.0_dp - theta_1
            tac2(:) = rotate(third_atom_coords,unit_vector,angle_2)
!
!           Now translate

            third_atom_coords = tac2 +  (/ x_0, y_0, z_0 /)
!
            x = third_atom_coords(1)
            y = third_atom_coords(2)
            z = third_atom_coords(3)

            
        elseif(atomNr.gt.2) then
          call Z2C(r,theta,phi,RelatedC,x,y,z)
        endif

      end subroutine Z2CGen

      subroutine Z2C(r,theta,phi,RelatedC,x,y,z)
!
!  Subroutine for generation of Cartesian coordinates from Z-Matrix
!
!  Julian Gale, NRI, Curtin University, March 2004
!
       use variables, only : dp, pi
        implicit none
!
!  Passed variables
!
        real(dp), intent(in)  :: r
        real(dp), intent(in)  :: theta
        real(dp), intent(in)  :: phi
        real(dp), intent(in)  :: RelatedC(3,3)
        real(dp), intent(out) :: x
        real(dp), intent(out) :: y
        real(dp), intent(out) :: z
!
!  Local variables
!
        real(dp)          :: rji
        real(dp)          :: rn
        real(dp)          :: rp
        real(dp)          :: xi
        real(dp)          :: yi
        real(dp)          :: zi
        real(dp)          :: xj
        real(dp)          :: yj
        real(dp)          :: zj
        real(dp)          :: xji
        real(dp)          :: yji
        real(dp)          :: zji
        real(dp)          :: xk
        real(dp)          :: yk
        real(dp)          :: zk
        real(dp)          :: xki
        real(dp)          :: yki
        real(dp)          :: zki
        real(dp)          :: xn
        real(dp)          :: yn
        real(dp)          :: zn
        real(dp)          :: xp
        real(dp)          :: yp
        real(dp)          :: zp
!
!  Find coordinates for related atoms
!
        xi = RelatedC(1,1)
        yi = RelatedC(2,1)
        zi = RelatedC(3,1)
!       vec_i = RelatedC(1:3,1)
        xj = RelatedC(1,2)
        yj = RelatedC(2,2)
        zj = RelatedC(3,2)
!       vec_j = RelatedC(1:3,2)
        xk = RelatedC(1,3)
        yk = RelatedC(2,3)
        zk = RelatedC(3,3)
!       vec_k = RelatedC(1:3,3)
!
!  Find unit vector along j->i vector
!
        xji = xi - xj
        yji = yi - yj
        zji = zi - zj
!       vec_ji = vec_i - vec_j
        rji = xji*xji + yji*yji + zji*zji
        rji = sqrt(rji)
        xji = xji/rji
        yji = yji/rji
        zji = zji/rji
!       call normalize(vec_ji)
!
!  Find j->k vector
!
        xki = xk - xj
        yki = yk - yj
        zki = zk - zj
!       vec_kj = vec_k - vec_j
!
!  Find unit vector normal to the i-j-k plane
!
        xn = yji*zki - yki*zji
        yn = zji*xki - zki*xji
        zn = xji*yki - xki*yji
!       vec_n = cross_product(vec_ji,vec_kj)
        rn = xn*xn + yn*yn + zn*zn
        rn = sqrt(rn)
        xn = xn/rn
        yn = yn/rn
        zn = zn/rn
!       call normalize(vec_n)
!
!  Find unit vector normal to the other 2 directions already found
!
!  Since original vectors are normalised the result should be likewise
!
        xp = yn*zji - yji*zn
        yp = zn*xji - zji*xn
        zp = xn*yji - xji*yn
!       vec_p = cross_product(vec_n,vec_ji)
!
!  Find distances along each unit vector
!
        rji = r*cos(theta)
        rn  = r*sin(theta)*sin(phi)
        rp  = r*sin(theta)*cos(phi)
!
!  Multiply unit vector by distances and add to origin to get position
!
!       rji enters with a minus sign as the convention for the angle is
!       that i is at the vertex, so an angle of less than pi/2 has the
!       atom "looking back" at j, and so opposite to vec_ji.
!
        x = xi - rji*xji + rn*xn + rp*xp
        y = yi - rji*yji + rn*yn + rp*yp
        z = zi - rji*zji + rn*zn + rp*zp
!       vec_pos = vec_i - rji*vec_ji + rn*vec_n + rp*vec_p
!       x = vec_pos(1)
!       y = vec_pos(2)
!       z = vec_pos(3)
!
      end subroutine Z2C

      function rotate(x,v,ang) result(y)
      use variables, only : dp
      implicit none
      real(dp), dimension(3), intent(in) :: x, v
      real(dp), intent(in)               :: ang
      real(dp), dimension(3)             :: y

      ! Use Rodrigues' formula to rotate a vector x around
      ! an axis defined by the vector v, by an angle 'ang'
      ! The rotation is counterclockwise looking down from
      ! the head to the foot of v.
      ! v does not need to be a unit vector
      ! See, for example,  Wikipedia

      real(dp)                :: cos_ang, sin_ang, dot_wx, v2
      real(dp), dimension(3)  :: cross_wx, w

      ! Construct a unit vector from v

      v2 = dot_product(v,v)
      if ( v2 == 0.0_dp) RETURN
      w  = v/sqrt(v2)

      cos_ang = cos(ang)
      sin_ang = sin(ang)
      cross_wx = cross_product(w,x)
      dot_wx = dot_product(w,x)
      y = x*cos_ang + cross_wx * sin_ang + dot_wx * w * ( 1.0_dp - cos_ang)

      end function rotate

      function cross_product(a,b) result(c)
      use variables, only : dp
      implicit none
      real(dp), dimension(3), intent(in) :: a, b
      real(dp), dimension(3)             :: c

      c(1) = a(2)*b(3) - a(3)*b(2)
      c(2) = a(3)*b(1) - a(1)*b(3)
      c(3) = a(1)*b(2) - a(2)*b(1)

      end function cross_product
end module zmtx2cart
