module optimal_paste

  use variables, only : ip, dp, cp, message, command_type, ref_molecule_type, num_mol_kinds, num_mol_of_kind &
                      , natoms, natoms0, label, pos, charge, nmols, molecule, mol_kind, pi
  use system_module, only : atm2mol
  implicit none
  integer(ip), allocatable, dimension (:) :: mol_paste_list
  type(ref_molecule_type), dimension (:), allocatable :: new_mol
  integer(ip) :: mol_paste_num

contains

  subroutine read_reference(upaste,nn,idx)
    implicit none
    integer(ip), intent(in) :: upaste
    integer(ip), intent(in) :: nn
    integer(ip), dimension(nn), intent(in) :: idx
    integer(ip) :: ikind, iatm, nat, imol, itmp, nw
    character(len=cp) :: label
    real(dp) :: xcom(3)
    character(len=100) :: line

#ifdef DEBUG
  write(0,*)"Entering subroutine :: read_reference"
#endif

! Read how many molecule kinds are to be substituted
!    read(upaste,'(a100)')line
!    call count_words(line,len(line),mol_paste_num) 
    mol_paste_num=nn

! Allocate arrays
    allocate(mol_paste_list(mol_paste_num)) ! list of molecules to substitute
    allocate(new_mol(mol_paste_num))        ! new molecules' templates

! Read which molecules are to be substituted
!    read(line,*)mol_paste_list(1:mol_paste_num)
    mol_paste_list(1:mol_paste_num) = idx

#ifdef DEBUG_OPASTE
    write(0,*)"mol_paste_list ",mol_paste_list(1:mol_paste_num)
#endif

! Read the references
    do imol=1,mol_paste_num

! Read the number of atoms of the new molecule
      read(upaste,*)nat
      new_mol(imol)%natoms=nat
      allocate(new_mol(imol)%pos(3,nat))
      allocate(new_mol(imol)%label(nat))
      allocate(new_mol(imol)%iref(nat))
      allocate(new_mol(imol)%chg(nat))
      new_mol(imol)%iref(nat)=0_ip
      new_mol(imol)%chg(nat)=0.0_dp

! Read the positions of the atoms in the reference molecule and the
! corrsponding atoms for the optimal paste
      itmp=0_ip
      xcom=0.0_dp

! XYZ comment line
      read(upaste,*)
      do iatm=1,nat
        read(upaste,'(a100)')line
        call count_words(line,len(line),nw)
        if (nw == 6) then
          read(line,*)new_mol(imol)%label(iatm),new_mol(imol)%pos(1:3,iatm),new_mol(imol)%chg(iatm),new_mol(imol)%iref(iatm)
        elseif (nw == 5) then
          read(line,*)new_mol(imol)%label(iatm),new_mol(imol)%pos(1:3,iatm),new_mol(imol)%iref(iatm)
        elseif (nw <= 4) then
          call message(0_ip,0_ip,0_ip,'Not enough columns in the ref molecule file',nw)
        endif
        if (new_mol(imol)%iref(iatm) > 0) then
          itmp=itmp+1_ip
          xcom(1:3)=xcom(1:3)+new_mol(imol)%pos(1:3,iatm)
        endif
      enddo 
      xcom=xcom/dble(itmp)

#ifdef DEBUG_OPASTE
      write(0,*)imol,new_mol(imol)%natoms
      do iatm=1,nat
        write(0,*)new_mol(imol)%label(iatm),new_mol(imol)%pos(1:3,iatm),new_mol(imol)%iref(iatm)
      enddo
      write(0,*)
#endif

! Set the reference centre of mass on the origin
      do iatm=1,nat
        new_mol(imol)%pos(1:3,iatm)=new_mol(imol)%pos(1:3,iatm)-xcom(1:3)
      enddo 
    enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_reference"
#endif

    return
  end subroutine read_reference

  subroutine do_optimal_paste(comms)
    implicit none
    type(command_type), intent(in) :: comms

    real(dp), allocatable, dimension (:,:) :: pos_new
    character(len=cp), allocatable, dimension (:)   :: label_new
    real(dp), allocatable, dimension (:) :: chg_new
    integer(ip), allocatable, dimension(:) :: atm2mol_new

    integer(ip) :: ikind, jkind
    integer(ip) :: iatm, jatm, nat, nmax, indx
    integer(ip) :: imol
    real(dp) :: rmsd, xcom(3), u(3), rotmat(3,3), molq(4)
    real(dp), allocatable, dimension (:,:) :: posmol, refmol

    logical, save :: first=.true.

    integer(ip) :: stat

#ifdef DEBUG
  write(0,*)"Entering subroutine :: do_optimal_paste"
#endif

! Count the new number of atoms
    nat = 0_ip
    outer1 : do ikind=1,num_mol_kinds
      do imol=1,mol_paste_num
        if( ikind==mol_paste_list(imol) )then
          nat=nat+new_mol(imol)%natoms*num_mol_of_kind(ikind)
          cycle outer1
        endif
      enddo
    enddo outer1

    do imol=1,nmols
      if (all(mol_paste_list/=molecule(imol)%kind)) nat=nat+molecule(imol)%natoms
    enddo
    allocate(pos_new(3,nat))
    allocate(label_new(nat))
    allocate(chg_new(nat))
    allocate(atm2mol_new(nat))

#ifdef DEBUG_OPASTE
    write(0,*)"old number of atoms",natoms
    write(0,*)"new number of atoms",nat
#endif

    nat = 0_ip
    do imol=1,nmols

! Nothing to do with this molecules
      if (all(mol_paste_list(1:mol_paste_num)/=molecule(imol)%kind)) then
        do iatm=1,molecule(imol)%natoms
          pos_new(1:3,nat+iatm) = pos(1:3,molecule(imol)%list(iatm))
          label_new(  nat+iatm) = label(  molecule(imol)%list(iatm))
          chg_new(    nat+iatm) = charge( molecule(imol)%list(iatm))
          atm2mol_new(nat+iatm) = atm2mol(molecule(imol)%list(iatm))
        enddo
        nat = nat + molecule(imol)%natoms
      else
        ikind=0
        do jkind=1,mol_paste_num
          if (mol_paste_list(jkind)==molecule(imol)%kind) then
            ikind=mol_paste_list(jkind)
            exit
          endif
        enddo 
        if (ikind==0) call message(0_ip,0_ip,0_ip,"Wrond molecule ID in optimal paste",ikind)

        nmax = count(new_mol(jkind)%iref(:)>0)
        allocate(posmol(3,nmax))
        allocate(refmol(3,nmax))

        xcom = 0.0_dp
        do iatm=1,new_mol(jkind)%natoms
          if (new_mol(jkind)%iref(iatm)==0) cycle
          indx = molecule(imol)%list(new_mol(jkind)%iref(iatm))
          xcom(1:3) = xcom(1:3) + pos(1:3,indx)
        enddo
        xcom(1:3)= xcom(1:3)/nmax 

        posmol = 0.0_dp
        jatm = 0_ip
        do iatm=1,new_mol(jkind)%natoms
          if (new_mol(jkind)%iref(iatm)==0) cycle
          jatm = jatm + 1_ip
          indx = molecule(imol)%list(new_mol(jkind)%iref(iatm))
          posmol(1:3,jatm) = pos(1:3,indx) - xcom(1:3)
        enddo

        refmol = 0.0_dp
        jatm=0_ip
        do iatm=1,new_mol(jkind)%natoms
          if (new_mol(jkind)%iref(iatm)==0) cycle
          jatm = jatm + 1_ip
          refmol(1:3,jatm) = new_mol(jkind)%pos(1:3,iatm)
        enddo

#ifdef DEBUG_OPASTE
        write(0,*)"Atoms in the old molecule used for pasting",nmax
        do iatm=1,nmax
          write(0,*)posmol(1:3,iatm)
        enddo
        write(0,*)""
        write(0,*)"Atoms in the new molecule used for pasting",nmax
        do iatm=1,nmax
          write(0,*)refmol(1:3,iatm)
        enddo
#endif

        call quat_align(nmax,refmol,posmol,rotmat,rmsd,molq)

        do iatm=1,new_mol(jkind)%natoms
          u(1:3)=new_mol(jkind)%pos(1:3,iatm)
          pos_new(1:3,nat+iatm) = xcom(1:3)+matmul(rotmat(1:3,1:3),u(1:3))
          label_new(nat+iatm) = new_mol(jkind)%label(iatm)
          chg_new(nat+iatm) = new_mol(jkind)%chg(iatm)
          atm2mol_new(nat+iatm) = atm2mol(indx)
        enddo

#ifdef DEBUG_OPASTE
        write(0,*)"rotmat"
        write(0,*)rotmat(:,1)
        write(0,*)rotmat(:,2)
        write(0,*)rotmat(:,3)
        write(0,*)"xcom : " , xcom
        write(0,*)"rmsd : " , rmsd
        write(0,*)"molq : " , molq
        write(0,*)"new molecule",new_mol(jkind)%natoms
        do iatm=1,new_mol(jkind)%natoms
          write(0,*)pos_new(1:3,nat+iatm),chg_new(nat+iatm)
        enddo
#endif

        nat = nat + new_mol(jkind)%natoms
        deallocate(posmol,refmol)
      endif

    enddo

    if (first .and. nat>natoms0) then
      deallocate(pos,label,charge,atm2mol)
      allocate(pos(3,nat))
      allocate(label(nat)) 
      allocate(charge(nat)) 
      allocate(atm2mol(nat)) 
      first=.false.
    endif
    do iatm=1,nat
      pos(1:3,iatm) = pos_new(1:3,iatm) 
      label(iatm) = label_new(iatm)
      charge(iatm) = chg_new(iatm)
      atm2mol(iatm) = atm2mol_new(iatm)
    enddo
     
    natoms=nat
    deallocate(pos_new,label_new,chg_new,atm2mol_new)

    call get_number_of_atoms_per_kind() 

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: do_optimal_paste"
#endif

    return
  end subroutine do_optimal_paste

  subroutine quat_align(nn,pos0,pos1,mat_dp,err_dp,q0_dp)
    implicit none
    integer(ip), intent(in) :: nn
    real(dp), dimension (3,nn), intent(in) :: pos1
    real(dp), dimension (3,nn), intent(in) :: pos0
    real(dp), dimension (3,3), intent(out) :: mat_dp
    real(dp), intent(out) :: err_dp
    real(dp), intent(out) :: q0_dp(4)
  
! precision necessary for the library
    integer :: i
    double precision :: s, epsi=1.d-10

    double precision :: rrsq, err
    double precision, dimension (3)  :: rr1, rr0
    double precision, dimension (3,3) :: mat
    double precision  :: q(0:3), m(4,4), lambda(4), z(4,4), q0(4)
 
#ifdef DEBUG
  write(0,*)"Entering subroutine :: do_quat_align"
#endif

    m=0.d0
    do i=1,nn
      rr0 =dble(pos0(:,i))
      rr1 =dble(pos1(:,i))
      rrsq=dble(sum(rr0*rr0)+sum(rr1*rr1))

      m(1,1)=m(1,1) + rrsq+2.d0*(-rr1(1)*rr0(1)-rr1(2)*rr0(2)-rr1(3)*rr0(3))
      m(2,2)=m(2,2) + rrsq+2.d0*(-rr1(1)*rr0(1)+rr1(2)*rr0(2)+rr1(3)*rr0(3))
      m(3,3)=m(3,3) + rrsq+2.d0*(+rr1(1)*rr0(1)-rr1(2)*rr0(2)+rr1(3)*rr0(3))
      m(4,4)=m(4,4) + rrsq+2.d0*(+rr1(1)*rr0(1)+rr1(2)*rr0(2)-rr1(3)*rr0(3))
      m(1,2)=m(1,2) +      2.d0*(-rr1(2)*rr0(3)+rr1(3)*rr0(2))
      m(1,3)=m(1,3) +      2.d0*( rr1(1)*rr0(3)-rr1(3)*rr0(1))
      m(1,4)=m(1,4) +      2.d0*(-rr1(1)*rr0(2)+rr1(2)*rr0(1))
      m(2,3)=m(2,3) -      2.d0*( rr1(1)*rr0(2)+rr1(2)*rr0(1))
      m(2,4)=m(2,4) -      2.d0*( rr1(1)*rr0(3)+rr1(3)*rr0(1))
      m(3,4)=m(3,4) -      2.d0*( rr1(2)*rr0(3)+rr1(3)*rr0(2))
    enddo

    m(2,1) = m(1,2)
    m(3,1) = m(1,3)
    m(3,2) = m(2,3)
    m(4,1) = m(1,4)
    m(4,2) = m(2,4)
    m(4,3) = m(3,4)

! Solve the eigenvector problem for m
    call jacobi(4_ip,m,lambda,z)
    call eigsrt(4_ip,lambda,z)

! Pick the correct eigenvector(s):
    s=1.d0
    if (z(1,1) .lt. 0.d0) then
      s = -1.d0
    endif
    q0(1:4)=z(1:4,1)*s
    q(0) = s*z(1,1)
    q(1) = s*z(2,1)
    q(2) = s*z(3,1)
    q(3) = s*z(4,1)

    if (abs(lambda(1)) .lt. epsi) then
       err = 0.d0
    else
       err = sqrt(lambda(1)/dble(nn))
    endif
    if (abs(lambda(1) - lambda(2)) .lt. epsi) call message(0_ip,0_ip,0_ip,'Quaternions : Not Converged',lambda(1))

    mat(1,1)=q(0)**2+q(1)**2-q(2)**2-q(3)**2
    mat(2,1)=2.d0*(q(1)*q(2)-q(0)*q(3))
    mat(3,1)=2.d0*(q(1)*q(3)+q(0)*q(2))
    mat(1,2)=2.d0*(q(1)*q(2)+q(0)*q(3))
    mat(2,2)=q(0)**2+q(2)**2-q(1)**2-q(3)**2
    mat(3,2)=2.d0*(q(2)*q(3)-q(0)*q(1))
    mat(1,3)=2.d0*(q(1)*q(3)-q(0)*q(2))
    mat(2,3)=2.d0*(q(2)*q(3)+q(0)*q(1))
    mat(3,3)=q(0)**2+q(3)**2-q(1)**2-q(2)**2

    mat_dp = real(mat,dp)
    err_dp = real(err,dp)
    q0_dp  = real(q0,dp)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: do_quat_align"
#endif

    return   
  end subroutine quat_align

end module optimal_paste
